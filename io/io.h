#ifndef LAB3_IO_H
#define LAB3_IO_H
#include <unistd.h>
#include <stdio.h>

#include "../queue/queue.h"

long fread_tmessage(FILE *fd, TMessage *msg);
void read_tmessage(int fd, TMessage *msg);
long fwrite_tmessage(TMessage *msg, FILE *fd);
int write_tmessage(TMessage *msg, int fd);

#endif