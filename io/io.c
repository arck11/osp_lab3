#include <errno.h>
#include "io.h"
#include "../help/help.h"

/*return read time*/
long fread_tmessage(FILE *fd, TMessage *msg) {
    long r_start = 0;

    uint64_t res = 0;
    errno = 0;

    while (res == 0) {
        res = fread(&(msg->Type), sizeof(uint8_t), 1, fd);
    }
    r_start = get_microtime();
    res = 0;
    while (res == 0) {
        res = fread(&(msg->Size), sizeof(uint64_t), 1, fd);
    }

    msg->Data = (uint8_t *) malloc(sizeof(uint8_t) * msg->Size);

    res = 0;

    while (res < msg->Size) {
        res += fread(msg->Data + res, sizeof(uint8_t), 1, fd);
    }
    return get_microtime() - r_start;
}

/*return write time*/
long fwrite_tmessage(TMessage *msg, FILE *fd) {
    uint64_t res;
    long w_start;

    w_start = get_microtime();

    res = fwrite(msg, sizeof(TMessageHeader), 1, fd);
    res += fwrite(msg->Data, sizeof(uint8_t), msg->Size, fd);

    return get_microtime() - w_start;
}
