//
// Created by Aidar Rakhimov on 15.11.2019.
//

#include <sys/time.h>
#include "help.h"

long get_microtime(){
    struct timeval currentTime;
    gettimeofday(&currentTime, NULL);
    return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}