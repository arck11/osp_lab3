#ifndef LAB3_UTILS_H
#define LAB3_UTILS_H

#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <zconf.h>

typedef enum {
    FIBONACCI,
    POW,
    BUBBLE_SORT_UINT64,
    STOP
} EType;

#pragma pack(push, 1)
typedef struct {
    uint8_t Type;
    uint64_t Size;
    uint8_t *Data;
} TMessage;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    uint8_t Type;
    uint64_t Size;
} TMessageHeader;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    EType Type;
    TMessage *Msg;
    uint64_t Size;
    uint8_t *Data;
} TMessageResult;
#pragma pack(pop)


typedef struct {
    EType Type;
    uint64_t Size;
    uint32_t *Data;
} SMessage;

typedef struct {
    long read;

    long queue_pre_push;
    long queue_pre_pop;

    long processing;

    long queue_post_push;
    long queue_post_pop;

    long write;
} TaskStat;

typedef struct {
    EType Type;
    uint64_t Size;
    uint32_t *Data;

    uint64_t RSize;
    uint64_t *Result;

    TaskStat stat;

} TaskMessage;

long long binpow(int a, int n);
long long fibonacci(int n);
void bubble_sort(uint64_t size, uint64_t *data);

TMessageResult* get_message_result(TMessage *msg);

void convert_message(TMessage *t_msg, SMessage *s_msg);

void init_task_stat(TaskStat* tm);
TaskMessage *get_task_message(SMessage *msg);

void freeTaskMessage(TaskMessage *task);

#endif