#include "tasks.h"


long long binpow(int a, int n) {
    long long test_cancel = 0;
    long long res = 1;
    while (n) {
        if (n & 1)
            res *= a;
        a *= a;
        n >>= 1;

        if (test_cancel++ % 1000 == 0) pthread_testcancel();
    }
    return res;
}

long long fibonacci(int n) {
    long long test_cancel = 0;
    long long a = 0, b = 1, tmp;

    while (n-- > 0) {
        tmp = a + b;
        a = b;
        b = tmp;

        if (test_cancel++ % 1000 == 0) pthread_testcancel();
    }

    return a;
}

void bubble_sort(uint64_t size, uint64_t *data) {
    uint64_t tmp;

    for (uint64_t i = 0; i < size; ++i) {
        for (uint64_t j = 0; j < size - i - 1; ++j) {
            if (data[j] > data[j + 1]) {
                tmp = data[j + 1];
                data[j + 1] = data[j];
                data[j] = tmp;
            }
        }

        pthread_testcancel();
    }
}

void init_task_stat(TaskStat *tm) {
    tm->processing = 0;
    tm->queue_post_pop = 0;
    tm->queue_post_push = 0;
    tm->queue_pre_pop = 0;
    tm->queue_pre_push = 0;
    tm->read = 0;
    tm->write = 0;
}

TaskMessage *get_task_message(SMessage *msg) {
    uint32_t* tdata;
    int len;

    TaskMessage *tm = (TaskMessage *) malloc(sizeof(TaskMessage));
    tm->Type = msg->Type;
    tm->Size = msg->Size;
    tm->RSize = msg->Size;
    len = (tm->Size < 1 ? 1 : tm->Size);
    tdata = (uint32_t*)malloc(sizeof(uint32_t) * len);
    tm->Data = tdata;
    for (int i = 0; i < tm->Size; ++i) {
        tm->Data[i] = msg->Data[i];
    }
    tm->Result = (uint64_t*)malloc(sizeof(uint64_t) * len);
    init_task_stat(&tm->stat);
    return tm;
}

void convert_message(TMessage *t_msg, SMessage *s_msg) {
    s_msg->Type = (EType) t_msg->Type;

    s_msg->Size = t_msg->Size >> 2;

    if (s_msg->Size > 0) {
        s_msg->Data = (uint32_t *) calloc(sizeof(uint32_t), s_msg->Size);

        for (int i = 0; i < t_msg->Size; i += 4) {
            s_msg->Data[i >> 2] = t_msg->Data[i] | (uint32_t) t_msg->Data[i + 1] << 8
                                 | (uint32_t) t_msg->Data[i + 2] << 16 | (uint32_t) t_msg->Data[i + 3] << 24;
        }
    }
}

void freeTaskMessage(TaskMessage *task) {
    free(task->Data);
    free(task->Result);
    free(task);
}