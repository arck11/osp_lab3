#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <getopt.h>
#include "tasks/tasks.h"
#include "queue/queue.h"
#include "io/io.h"
#include "thread_pool/thread_pool.h"

#include "strategy/strategy.h"
#include "worker/worker.h"

struct CLArgs {
    strategy_t strategy;
    long threads_count;
    long report_timeout;
};

struct CLArgs *parse_argumets(int argc, char **argv, struct CLArgs *args) {

    static struct option long_options[] =
            {
                    /* These options set a flag. */
                    {"strategy",      required_argument, NULL, 's'},
                    {"count-threads", optional_argument, NULL, 'c'},
                    {"report-timeout", optional_argument, NULL, 'n'},
                    {0, 0, 0,                                  0}

            };
    /* getopt_long stores the option index here. */
    int option_index = 0;
    int opt= 0, err = 0;
    char *optstring = "s:c:n:";
    args->report_timeout = 100;
    optind = 0;
    while ((opt = getopt_long(argc, argv, optstring, long_options, &option_index)) != -1) {
        switch (opt) {
            case 's' :
                if (strcmp(optarg, STRATEGY_PER_THREAD) == 0) {
                    args->strategy = per_thread;
                } else if (strcmp(optarg, STRATEGY_PER_TASK) == 0) {
                    args->strategy = per_task;
                } else if (strcmp(optarg, STRATEGY_THREAD_POOL) == 0) {
                    args->strategy = thread_pool;
                } else {
                    err = argument_with_invalid_value;
                }
                break;
            case 'c' :
                args->threads_count = strtol(argv[optind], NULL, 10);
                break;
            case 'n':
                args->report_timeout = strtol(argv[optind], NULL, 10);
                break;
            default:
                exit(EXIT_FAILURE);
        }
    }
}

int main(int argc, char **argv) {

    struct CLArgs args;
    parse_argumets(argc, argv, &args);

//    FILE *f = fopen("input.txt", "r+");
    TMessage msg;
    SMessage smsg;

    process(args.strategy, (int)args.threads_count, stdin, args.report_timeout);

//    fclose(f);
    return EXIT_SUCCESS;
}

