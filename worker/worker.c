#include <errno.h>
#include "worker.h"
#include "../help/help.h"
#include "../thread_pool/thread_pool.h"
#include "../stat/stat.h"


void *writer(void *args) {
    Queue *queue = ((struct PerTaskArgs *) args)->from_q;
    Queue *stat_queue = ((struct PerTaskArgs *) args)->to_q;

    long start_time;
    FILE *file = fopen("_writer_output.txt", "w+");

    TaskMessage *msg;

    pthread_cleanup_push(fclose, file)

    while (true) {
        msg = (TaskMessage *) popQueue(queue);
        msg->stat.queue_post_pop = get_microtime();

        if (msg->Type == STOP) {
            continue;
        }

        start_time = get_microtime();

        fprintf(file, "Type: %d, MsgSize: %lld, Data: ", msg->Type, msg->Size);

        for (uint64_t i = 0; i < msg->Size; ++i) {
            fprintf(file, "%d ", msg->Data[i]);
        }

        fprintf(file, "\nResultSize: %lld, Data: ", msg->RSize);

        for (uint64_t i = 0; i < msg->RSize; ++i) {
            fprintf(file, "%lld ", msg->Result[i]);
        }

        fprintf(file, "\n");

        msg->stat.write = get_microtime() - start_time;

        pushQueue(stat_queue, msg);
        pthread_testcancel();
    }

    pthread_cleanup_pop(0)
    fclose(file);
}

pthread_t* start_writer(Queue* result_queue, Queue* stat_queue) {
    pthread_t *th;
    struct PerTaskArgs *args;
    th = (pthread_t *) malloc(sizeof(pthread_t *));
    args = (struct PerTaskArgs *) malloc(sizeof(struct PerTaskArgs));
    args->from_q = result_queue;
    args->to_q = stat_queue;
    pthread_create(th, NULL, writer, (void *) args);
    return th;
}

void task_handler(TaskMessage *msg) {
    switch (msg->Type) {
        case FIBONACCI:
            msg->stat.processing = get_microtime();

            msg->Result[0] = fibonacci(msg->Data[0]);

            msg->stat.processing = get_microtime() - msg->stat.processing;
            break;
        case POW:
            msg->stat.processing = get_microtime();

            msg->Result[0] = binpow(msg->Data[0], msg->Data[1]);

            msg->stat.processing = get_microtime() - msg->stat.processing;
            msg->RSize = 1;
            break;
        case BUBBLE_SORT_UINT64:
            for (int i = 0; i < msg->Size; ++i) {
                msg->Result[i] = msg->Data[i];
            }
            msg->stat.processing = get_microtime();

            bubble_sort(msg->Size, msg->Result);

            msg->stat.processing = get_microtime() - msg->stat.processing;
            break;
        case STOP:
            break;
        default:
            fprintf(stderr, "Invalid type\n");
            break;
    }
}

void *thread_handler(void *args) {

    TaskMessage *msg = ((struct thread_args *) args)->msg;
    Queue *result_queue = ((struct thread_args *) args)->result_queue;

    msg->stat.queue_pre_pop = get_microtime();

    task_handler(msg);

    pushQueue(result_queue, msg);
    msg->stat.queue_post_push = get_microtime();
}


void process_per_thread(FILE *fd, Queue *result_queue) {

    TMessage tmsg;
    SMessage smsg;
    TaskMessage *task;
    long read_time = 0;

    Queue *threads = createQueue();
    pthread_t *th;

    struct thread_args *args;

    while (true) {

        read_time = fread_tmessage(fd, &tmsg);
        convert_message(&tmsg, &smsg);

        task = get_task_message(&smsg);
        task->stat.read = read_time;

        if (task->Type == STOP) {
            break;
        }

        th = (pthread_t *) malloc(sizeof(pthread_t *));
        args = (struct thread_args *) malloc(sizeof(struct thread_args));

        args->msg = task;
        args->result_queue = result_queue;

        pushQueue(threads, th);
        task->stat.queue_pre_push = get_microtime();
        pthread_create(th, NULL, thread_handler, (void *) args);
    }

    while (!isEmptyQueue(threads)) {
        th = popQueue(threads);
        pthread_join(*th, NULL);
        free(th);
    }

    pushQueue(result_queue, task);

    freeQueue(threads);
}

void *per_task_handler(void *args) {
    TaskMessage *task;
    bool work = true;

    Queue *from_q = ((struct PerTaskArgs *) args)->from_q;
    Queue *to_q = ((struct PerTaskArgs *) args)->to_q;

    while (work) {
        task = (TaskMessage *) popQueue(from_q);
        task->stat.queue_pre_pop = get_microtime();

        if (task->Type == STOP) {
            work = false;
        }

        task_handler(task);

        pushQueue(to_q, (void *) task);
        task->stat.queue_post_push = get_microtime();

        pthread_testcancel();
    }

    free(args);

}

void process_per_task(FILE *fd, Queue *result_queue) {
    TMessage tmsg;
    SMessage smsg;
    TaskMessage *task;
    long read_time = 0;
    struct PerTaskArgs *args;
    bool work = true;

    Queue *threads = createQueue();

    Queue *fib_q = createQueue();
    Queue *pow_q = createQueue();
    Queue *sort_q = createQueue();

    pthread_t *th;

    th = (pthread_t *) malloc(sizeof(pthread_t *));
    args = (struct PerTaskArgs *) malloc(sizeof(struct PerTaskArgs));
    args->from_q = fib_q;
    args->to_q = result_queue;
    pushQueue(threads, th);
    pthread_create(th, NULL, per_task_handler, (void *) args);

    th = (pthread_t *) malloc(sizeof(pthread_t *));
    args = (struct PerTaskArgs *) malloc(sizeof(struct PerTaskArgs));
    args->from_q = pow_q;
    args->to_q = result_queue;
    pushQueue(threads, th);
    pthread_create(th, NULL, per_task_handler, (void *) args);

    th = (pthread_t *) malloc(sizeof(pthread_t *));
    args = (struct PerTaskArgs *) malloc(sizeof(struct PerTaskArgs));
    args->from_q = sort_q;
    args->to_q = result_queue;
    pushQueue(threads, th);
    pthread_create(th, NULL, per_task_handler, (void *) args);

    while (work) {

        read_time = fread_tmessage(fd, &tmsg);
        convert_message(&tmsg, &smsg);

        task = get_task_message(&smsg);
        task->stat.read = read_time;
        task->stat.queue_pre_push = get_microtime();
        switch (task->Type) {
            case FIBONACCI:
                pushQueue(fib_q, task);
                break;
            case POW:
                pushQueue(pow_q, task);
                break;
            case BUBBLE_SORT_UINT64:
                pushQueue(sort_q, task);
                break;
            case STOP:
                work = false;
                break;
            default:
                fprintf(stderr, "Invalid type\n");
                break;
        }
    }

    while(!isEmptyQueue(fib_q));
    while(!isEmptyQueue(pow_q));
    while(!isEmptyQueue(sort_q));

    while (!isEmptyQueue(threads)) {
        th = popQueue(threads);
        pthread_cancel(*th);
        pthread_join(*th, NULL);
        free(th);
    }

    pushQueue(result_queue, task);

    freeQueue(threads);
}

void process_thread_pool(FILE *fd, int threads_count, Queue *result_queue) {
    TMessage tmsg;
    SMessage smsg;
    TaskMessage *task;
    long read_time = 0;
    struct thread_args *args;

    bool work = true;
    thread_pool_t *thread_pool = thread_pool_create(threads_count);

    while (work) {
        read_time = fread_tmessage(fd, &tmsg);
        convert_message(&tmsg, &smsg);

        task = get_task_message(&smsg);
        task->stat.read = read_time;

        if (task->Type == STOP) {
            work = false;
        }

        args = (struct thread_args *) malloc(sizeof(struct thread_args));

        args->msg = task;
        args->result_queue = result_queue;

        task->stat.queue_pre_push = get_microtime();
        thread_pool_add(thread_pool, thread_handler, (void*)args);
    }

    thread_pool_destroy(thread_pool, 1);
}

int process(strategy_t strategy, int threads_count, FILE *fd, long report_timeout) {
    Queue *result_queue = createQueue();
    Queue *stat_queue = createQueue();
    pthread_t* writer_th;
    pthread_t* stat_th;

    writer_th = start_writer(result_queue, stat_queue);
    stat_th = start_stat(stat_queue, report_timeout);

    switch (strategy) {
        case per_thread:
            process_per_thread(fd, result_queue);
            break;
        case per_task:
            process_per_task(fd, result_queue);
            break;
        case thread_pool:
            process_thread_pool(fd, threads_count, result_queue);
            break;
        default:
            fprintf(stderr, "Invalid strategy\n");
            return 1;
    }

    while(!isEmptyQueue(result_queue));
    while(!isEmptyQueue(stat_queue));

    pthread_cancel(*writer_th);
    pthread_join(*writer_th, NULL);

    pthread_cancel(*stat_th);
    pthread_join(*stat_th, NULL);

    freeQueue(result_queue);
    freeQueue(stat_queue);

    return 0;
}