#ifndef WORKER_H
#define WORKER_H

#include "../strategy/strategy.h"
#include "../queue/queue.h"
#include "../io/io.h"

#include <unistd.h>
#include <stdio.h>

struct thread_args {
    TaskMessage *msg;
    Queue *result_queue;
};

struct PerTaskArgs {
    Queue *from_q;
    Queue *to_q;
};

int process(strategy_t strategy, int threads_count, FILE *fd, long report_timeout);
void task_handler(TaskMessage *msg);
pthread_t* start_writer(Queue* result_queue, Queue* stat_queue);
void *thread_handler(void *args);


void process_per_thread(FILE *fd, Queue *result_queue);
void process_per_task(FILE *fd, Queue *result_queue);
void process_thread_pool(FILE *fd, int threads_count, Queue *result_queue);

#endif