//
// Created by Aidar Rakhimov on 15.11.2019.
//

#ifndef OSP_LAB3_STAT_H
#define OSP_LAB3_STAT_H

#include <stdlib.h>
#include <math.h>
#include <stdlib.h>
#include "../help/help.h"
#include "../queue/queue.h"
#include "../tasks/tasks.h"
#include "../worker/worker.h"

#define INITIAL_CAPACITY 100

typedef struct {
    int size;
    int capacity;
    long* arr;
} DynamicArray;

int push(DynamicArray* darr, int index, long value);

typedef struct {
    int count;
    DynamicArray rd, wr, process, pre_q, post_q;
} Stats;

struct StatTh {
    Queue *queue;
    long report_timeout;
};

int initDArray(DynamicArray* darr);
int initStat(Stats* stats);


void sort_array(long* values, int size);
long double percentile(long* values, int size, double percent);
void* stat_writer(void *stat_queue);
pthread_t* start_stat(Queue* result_queue, long report_timeout);
int push_stat(TaskStat *stat, Stats *stats);
void part_bubble_sort(long* value, int size);

#endif //OSP_LAB3_STAT_H
