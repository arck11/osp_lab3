//
// Created by Aidar Rakhimov on 15.11.2019.
//

#include "stat.h"

void part_bubble_sort(long *values, int size) {
    int flag = 0;
    long tmp;
    int i, j;

    for (i = 0; i < size; i++) {
        flag = 0;
        for (j = 0; j < size - 1; j++) {
            if (values[j] > values[j + 1]) {
                tmp = values[j + 1];
                values[j + 1] = values[j];
                values[j] = tmp;
                flag = 1;
            }
        }

        if (flag == 0) {
            break;
        }
    }
}

long double percentile(long *values, int size, double percent) {
    long double k, d0, d1;
    long c, f;

    k = (size - 1) * percent;
    c = ceil(k);
    f = floor(k);

    if (c == f) {
        return (long double) values[c];
    }

    d0 = values[f] * (c - k);
    d1 = values[c] * (k - f);

    return d0 + d1;
}


int push(DynamicArray *arr, int index, long value) {
    long *tmp;
    size_t new_cap;

    while (index > arr->capacity) {
        new_cap = arr->capacity * 2;
        tmp = malloc(sizeof(long) * new_cap);

        if (tmp != NULL) {
            for (int i = 0; i < arr->capacity; ++i) {
                tmp[i] = arr->arr[i];
            }
            arr->arr = tmp;
        } else {
            fprintf(stderr, "realloc failed\n");
            return 1;
        }

        arr->capacity *= 2;
    }
    arr->arr[index] = value;
    arr->size++;
    return 0;
}

int initDArray(DynamicArray *darr) {
    darr->size = 0;
    darr->capacity = INITIAL_CAPACITY;
    darr->arr = (long *) malloc(sizeof(long) * darr->capacity);
}

int initStat(Stats *stats) {
    initDArray(&stats->rd);
    initDArray(&stats->wr);
    initDArray(&stats->process);
    initDArray(&stats->pre_q);
    initDArray(&stats->post_q);
    stats->count = 0;
}

int push_stat(TaskStat *stat, Stats *stats) {
    int result = 0;

    result |= push(&(stats->rd), stats->count, stat->read);
    result |= push(&(stats->wr), stats->count, stat->write);
    result |= push(&(stats->pre_q), stats->count, stat->queue_pre_pop - stat->queue_pre_push);
    result |= push(&(stats->post_q), stats->count, stat->queue_post_pop - stat->queue_post_push);
    result |= push(&(stats->process), stats->count, stat->processing);

    if (result == 0) {
        stats->count++;
    }

    return result;
}

void print_stats(FILE *fd, Stats *stats) {

    if (stats == NULL) {
        return;
    }

    part_bubble_sort(stats->rd.arr, stats->rd.size);
    part_bubble_sort(stats->wr.arr, stats->wr.size);
    part_bubble_sort(stats->process.arr, stats->process.size);
    part_bubble_sort(stats->pre_q.arr, stats->pre_q.size);
    part_bubble_sort(stats->post_q.arr, stats->post_q.size);

    fprintf(fd, "%Lf %Lf %Lf %Lf %Lf | ",
            percentile(stats->rd.arr, stats->rd.size, 0.1),
            percentile(stats->rd.arr, stats->rd.size, 0.25),
            percentile(stats->rd.arr, stats->rd.size, 0.50),
            percentile(stats->rd.arr, stats->rd.size, 0.75),
            percentile(stats->rd.arr, stats->rd.size, 0.90)
    );
    fprintf(fd, "%Lf %Lf %Lf %Lf %Lf | ",
            percentile(stats->pre_q.arr, stats->pre_q.size, 0.1),
            percentile(stats->pre_q.arr, stats->pre_q.size, 0.25),
            percentile(stats->pre_q.arr, stats->pre_q.size, 0.50),
            percentile(stats->pre_q.arr, stats->pre_q.size, 0.75),
            percentile(stats->pre_q.arr, stats->pre_q.size, 0.90)
    );
    fprintf(fd, "%Lf %Lf %Lf %Lf %Lf | ",
            percentile(stats->process.arr, stats->process.size, 0.1),
            percentile(stats->process.arr, stats->process.size, 0.25),
            percentile(stats->process.arr, stats->process.size, 0.50),
            percentile(stats->process.arr, stats->process.size, 0.75),
            percentile(stats->process.arr, stats->process.size, 0.90)
    );
    fprintf(fd, "%Lf %Lf %Lf %Lf %Lf | ",
            percentile(stats->post_q.arr, stats->post_q.size, 0.1),
            percentile(stats->post_q.arr, stats->post_q.size, 0.25),
            percentile(stats->post_q.arr, stats->post_q.size, 0.50),
            percentile(stats->post_q.arr, stats->post_q.size, 0.75),
            percentile(stats->post_q.arr, stats->post_q.size, 0.90)
    );
    fprintf(fd, "%Lf %Lf %Lf %Lf %Lf | ",
            percentile(stats->wr.arr, stats->wr.size, 0.1),
            percentile(stats->wr.arr, stats->wr.size, 0.25),
            percentile(stats->wr.arr, stats->wr.size, 0.50),
            percentile(stats->wr.arr, stats->wr.size, 0.75),
            percentile(stats->wr.arr, stats->wr.size, 0.90)
    );
    fprintf(fd, "%Lf\n", percentile(stats->rd.arr, stats->rd.size, 0.90)
                         + percentile(stats->pre_q.arr, stats->pre_q.size, 0.90)
                         + percentile(stats->process.arr, stats->process.size, 0.90)
                         + percentile(stats->post_q.arr, stats->post_q.size, 0.90)
                         + percentile(stats->wr.arr, stats->wr.size, 0.90)
    );


}

void *stat_writer(void *args) {
    Queue *queue = ((struct StatTh *) args)->queue;
    long report_timeout = ((struct StatTh *) args)->report_timeout;
    long last_report = 0;
    TaskMessage *msg;
    Stats stats;

    initStat(&stats);
    last_report = get_microtime();
    FILE *stat_f = fopen("_stats_output.txt", "w+");


    while (true) {
        msg = (TaskMessage *) popQueue(queue);
        msg->stat.queue_post_pop = get_microtime();

        if (msg->Type == STOP) {
            continue;
        }

        push_stat(&(msg->stat), &stats);

        if (get_microtime() - last_report < report_timeout * 1000) {
            print_stats(stat_f, &stats);
        }

        pthread_testcancel();

    }
    fclose(stat_f);
}


pthread_t *start_stat(Queue *result_queue, long report_timeout) {
    pthread_t *th;
    th = (pthread_t *) malloc(sizeof(pthread_t *));

    struct StatTh *args;
    args = (struct StatTh *) malloc(sizeof(struct StatTh));
    args->queue = result_queue;
    args->report_timeout = report_timeout;

    pthread_create(th, NULL, stat_writer, (void *) args);
    return th;
}



