#ifndef STRATEGY_H
#define STRATEGY_H

typedef enum {
    per_thread = 0,
    per_task = 1,
    thread_pool = 2
} strategy_t;

#define STRATEGY_KEY "--strategy"
#define THREADS_COUNT_KEY "--count-threads"

#define STRATEGY_PER_THREAD "per_thread"
#define STRATEGY_PER_TASK "per_task"
#define STRATEGY_THREAD_POOL "thread_pool"


typedef enum  {
    invalid_arguments = 1,
    incorrect_arguments_count = 2,
    argument_with_invalid_value = 3
} strategy_errors;


#endif