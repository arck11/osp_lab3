CC = gcc
CFLAGS = -std=gnu99 -Wall -g

TARGET = worker

SRC = main.c tasks/tasks.c io/io.c queue/queue.c thread_pool/thread_pool.c help/help.c strategy/strategy.c worker/worker.c stat/stat.c
OBJ = $(SRC:%.c=%.o)

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(OBJ)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm -f $(OBJ) $(TARGET)


coverage:
	clang -fprofile-instr-generate -fcoverage-mapping -v $(SRC) -o "$(TARGET)_coverage"

coverage2:
	LLVM_PROFILE_FILE="$(TARGET)_coverage.profraw" ./$(TARGET)_coverage

coverage3:
	llvm-profdata merge -sparse ./$(TARGET)_coverage.profraw -o $(TARGET)_coverage.profdata
coverage4:
	llvm-cov show ./$(TARGET)_coverage -instr-profile="$(TARGET)_coverage.profdata"