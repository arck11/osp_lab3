//
// Created by Aidar Rakhimov on 20.11.2019.
//
#include <assert.h>
#include <stdio.h>
#include "../queue/queue.h"
#include "../help/help.h"
#include "../io/io.h"
#include "../thread_pool/thread_pool.h"
#include "../tasks/tasks.h"
#include "../stat/stat.h"
#include "../worker/worker.h"

const long double EPS = 1E-7;

void test_queue() {
    Queue *q;
    q = createQueue();
    assert(isEmptyQueue(q));
    assert(!isOneQueue(q));

    int *value, *second_v;
    int *pop_value;

    value = (int*)malloc(sizeof(int));
    second_v = (int*)malloc(sizeof(int));

    *value = 13;
    *second_v = 17;

    pushQueue(q, value);
    assert(!isEmptyQueue(q));
    assert(isOneQueue(q));

    pushQueue(q, second_v);

    assert(!isEmptyQueue(q));
    assert(!isOneQueue(q));

    pop_value = popQueue(q);

    assert(pop_value == value);
    assert(*pop_value == *value);

    pop_value = popQueue(q);

    assert(pop_value == second_v);
    assert(*pop_value == *second_v);

    int *a, *b;

    a = (int*)malloc(sizeof(int));
    b = (int*)malloc(sizeof(int));

    *a = 1;
    *b = 2;

    pushQueue(q, a);
    pushQueue(q, b);

    freeQueue(q);

    printf("%s +\n", "test_queue");
}

void test_help() {
    long t = get_microtime();

    assert(t > 0L);
    printf("%s +\n", "test_help");
}

void test_io() {
    FILE *f;
    TMessage msg, msg2;
    long result = 0;

    msg.Type = 0;
    msg.Size = 4;
    msg.Data = (uint8_t*)calloc(sizeof(uint8_t), msg.Size);

    msg.Data[0] = 1;
    msg.Data[1] = 2;
    msg.Data[2] = 3;
    msg.Data[3] = 4;

    msg2.Type = 1;
    msg2.Size = 8;
    msg2.Data = (uint8_t*)calloc(sizeof(uint8_t), msg.Size);

    msg2.Data[0] = 4;
    msg2.Data[1] = 3;
    msg2.Data[2] = 2;
    msg2.Data[3] = 1;

    f = fopen("test.dat", "w+");
    result = fwrite_tmessage(&msg, f);
    fclose(f);

    f = fopen("test.dat", "r+");
    result = fread_tmessage(f, &msg2);
    fclose(f);

    assert(msg2.Type == msg.Type);
    assert(msg2.Size == msg.Size);

    for (int i = 0; i < msg.Size; ++i) {
        assert(msg.Data[i] == msg2.Data[i]);
    }
    printf("%s +\n", "test_io");
}


pthread_mutex_t _tp_lock;
int _tp_count = 0;

void* _test(void* value) {
    pthread_mutex_lock(&_tp_lock);

    _tp_count += *(int*)value;

    pthread_mutex_unlock(&_tp_lock);

}

void test_thread_pool() {
    thread_pool_t *tp;
    _tp_count = 0;
    int value = 1, result = -1;
    pthread_mutex_init(&_tp_lock, NULL);

    tp = thread_pool_create(2);

    assert(tp != NULL);

    thread_pool_add(tp, _test, &value);

    usleep(100);
    assert(_tp_count == value);

    thread_pool_add(tp, _test, &value);

    usleep(100);
    assert(_tp_count == 2 * value);

    result = thread_pool_destroy(tp, 1);

    assert(result == 0);

    assert(thread_pool_add(NULL, NULL, NULL) == threadpool_invalid);
    assert(thread_pool_destroy(NULL, 1) == threadpool_invalid);
    assert(thread_pool_free(NULL) == -1);

    printf("%s +\n", "test_thread_pool");
}


void test_tasks() {
    long long a, b;
    int x, y;

    uint64_t s = 0;

    uint64_t arr[1000];

    for (int i = 0; i < 1000; ++i) {
        arr[i] = 1000 - i - 1;
    }

    assert(binpow(3, 7) == 2187);
    assert(binpow(2, 7) == 128);
    assert(binpow(7, 7) == 823543);

    assert(fibonacci(8) == 21);
    assert(fibonacci(13) == 233);
    assert(fibonacci(17) == 1597);
    assert(fibonacci(56) == 225851433717);

    bubble_sort(1000, arr);

    for (int i = 0; i < 1000; ++i) {
        assert(arr[i] == i);
    }

    TMessage msg;
    msg.Type = 0;
    msg.Size = 4;
    msg.Data = (uint8_t*)malloc(sizeof(uint8_t) * msg.Size);

    msg.Data[0] = 1;
    msg.Data[1] = 2;
    msg.Data[2] = 3;
    msg.Data[3] = 4;

    TaskStat tm;
    tm.write = 1;
    tm.queue_post_pop = 1;
    tm.queue_pre_push = 1;
    tm.queue_post_push = 1;
    tm.queue_pre_pop = 1;
    tm.read = 1;
    tm.processing = 1;

    init_task_stat(&tm);

    assert(tm.write == 0);
    assert(tm.queue_post_pop == 0);
    assert(tm.queue_pre_push == 0);
    assert(tm.queue_post_push == 0);
    assert(tm.queue_pre_pop == 0);
    assert(tm.read == 0);
    assert(tm.processing == 0);

    TaskMessage *task_msg;
    SMessage s_msg;

    convert_message(&msg, &s_msg);

    assert(s_msg.Type == msg.Type);
    assert(s_msg.Data[0] == 67305985);
    assert(s_msg.Size == 1);

    task_msg = get_task_message(&s_msg);

    assert(task_msg->Type == s_msg.Type);
    assert(task_msg->Size == s_msg.Size);
    assert(task_msg->RSize == s_msg.Size);

    for (int j = 0; j < task_msg->Size; ++j) {
        assert(task_msg->Data[j] == s_msg.Data[j]);
    }

    assert(task_msg->stat.write == 0);
    assert(task_msg->stat.queue_post_pop == 0);
    assert(task_msg->stat.queue_pre_push == 0);
    assert(task_msg->stat.queue_post_push == 0);
    assert(task_msg->stat.queue_pre_pop == 0);
    assert(task_msg->stat.read == 0);
    assert(task_msg->stat.processing == 0);

    freeTaskMessage(task_msg);


    printf("%s +\n", "test_tasks");
}


void test_stat() {
    Stats stats;
    TaskStat tm;

    initStat(&stats);
    init_task_stat(&tm);

    assert(stats.count == 0);
    assert(stats.wr.size == 0);
    assert(stats.rd.size == 0);
    assert(stats.process.size == 0);
    assert(stats.post_q.size == 0);
    assert(stats.pre_q.size == 0);
    assert(stats.wr.capacity == INITIAL_CAPACITY);
    assert(stats.rd.capacity == INITIAL_CAPACITY);
    assert(stats.process.capacity == INITIAL_CAPACITY);
    assert(stats.post_q.capacity == INITIAL_CAPACITY);
    assert(stats.pre_q.capacity == INITIAL_CAPACITY);

    long new_cap = 2 * INITIAL_CAPACITY;

    for (int i = 0; i < new_cap; ++i) {
        tm.read = new_cap - i;
        tm.write = new_cap - i;
        tm.processing = new_cap - i;
        tm.queue_pre_pop = new_cap - i;
        tm.queue_post_pop = new_cap - i;
        assert(push_stat(&tm, &stats) == 0);
    }
    assert(stats.count == new_cap);
    assert(stats.wr.size == new_cap);
    assert(stats.rd.size == new_cap);
    assert(stats.process.size == new_cap);
    assert(stats.post_q.size == new_cap);
    assert(stats.pre_q.size == new_cap);
    assert(stats.wr.capacity == new_cap);
    assert(stats.rd.capacity == new_cap);
    assert(stats.process.capacity == new_cap);
    assert(stats.post_q.capacity == new_cap);
    assert(stats.pre_q.capacity == new_cap);

    part_bubble_sort(stats.rd.arr, stats.rd.size);

    for (int ww = 0; ww < stats.rd.size - 1; ww++) {
        assert(stats.rd.arr[ww] < stats.rd.arr[ww + 1]);
    }

    assert(abs(percentile(stats.rd.arr, stats.rd.size, 0.5) - 100.0) < EPS);
    assert(abs(percentile(stats.rd.arr, stats.rd.size, 0.9) - 180.0) < EPS);
    assert(abs(percentile(stats.rd.arr, stats.rd.size, 0.75) - 150.0) < EPS);


    printf("%s +\n", "test_stat");
}

void test_worker() {
    TaskMessage *task_msg, *tmp_task;
    SMessage s_msg;
    TMessage msg;
    msg.Type = 0;
    msg.Size = 8;
    msg.Data = (uint8_t*)malloc(sizeof(uint8_t) * msg.Size);

    msg.Data[0] = 13;
    msg.Data[1] = 0;
    msg.Data[2] = 0;
    msg.Data[3] = 0;
    msg.Data[4] = 7;
    msg.Data[5] = 0;
    msg.Data[6] = 0;
    msg.Data[7] = 0;

    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

    task_handler(task_msg);

    assert(task_msg->Result[0] == 233);

    msg.Type = 1;

    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

    task_handler(task_msg);

    assert(task_msg->Result[0] == 62748517);

    msg.Type = 2;

    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

    task_handler(task_msg);

    assert(task_msg->Result[0] == 7);
    assert(task_msg->Result[1] == 13);

    msg.Type = 3;
    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

    task_handler(task_msg);

//    msg.Type = 4;
//    convert_message(&msg, &s_msg);
//    task_msg = get_task_message(&s_msg);
//
//    task_handler(task_msg);

    Queue *q;
    Queue *stat_q;
    q = createQueue();
    stat_q = createQueue();

    msg.Type = 1;

    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

//    pthread_t* th;
//    th = start_writer(q, stat_q);
//
//    assert(th != NULL);
//
//    pushQueue(q, task_msg);
//
//    msg.Type = 3;
//
//    convert_message(&msg, &s_msg);
//    task_msg = get_task_message(&s_msg);
//    pushQueue(q, task_msg);
//
//    usleep(1000);
//    pthread_cancel(*th);
//    pthread_join(*th, NULL);
//
//    freeQueue(q);
    q = createQueue();

    msg.Type = 0;

    convert_message(&msg, &s_msg);
    task_msg = get_task_message(&s_msg);

    struct thread_args *args;
    args = (struct thread_args *) malloc(sizeof(struct thread_args));

    args->msg = task_msg;
    args->result_queue = q;

    thread_handler(args);

    tmp_task = popQueue(q);

    assert(tmp_task->Size == task_msg->Size);
    assert(tmp_task == task_msg);
    assert(tmp_task->Result[0] == 233);

    FILE *f;

    f = fopen("tmp_test_worker.dat", "w+");

    msg.Type = 0;
    fwrite_tmessage(&msg, f);
    msg.Type = 1;
    fwrite_tmessage(&msg, f);
    msg.Type = 2;
    fwrite_tmessage(&msg, f);
    msg.Type = 3;
    fwrite_tmessage(&msg, f);

    fclose(f);

    f = fopen("tmp_test_worker.dat", "r+");

    q = createQueue();

    process_per_task(f, q);

    tmp_task = popQueue(q);
    tmp_task = popQueue(q);
    tmp_task = popQueue(q);

    assert(isOneQueue(q));

    tmp_task = popQueue(q);

    assert(isEmptyQueue(q));

    fseek( f, 0, SEEK_SET );

    process_thread_pool(f, 1, q);

    tmp_task = popQueue(q);
    tmp_task = popQueue(q);
    tmp_task = popQueue(q);

    assert(isOneQueue(q));

    tmp_task = popQueue(q);

    assert(isEmptyQueue(q));

    fseek( f, 0, SEEK_SET );

    process_per_thread(f, q);

    tmp_task = popQueue(q);
    tmp_task = popQueue(q);
    tmp_task = popQueue(q);

    assert(isOneQueue(q));

    tmp_task = popQueue(q);

    assert(isEmptyQueue(q));

    fclose(f);


    printf("%s +\n", "test_worker");
}

int main() {
    long stop = 2000;

    test_queue();
    usleep(stop);

    test_help();
    usleep(stop);

    test_io();
    usleep(stop);

    test_thread_pool();
    usleep(stop);

    test_tasks();
    usleep(stop);

    test_worker();
    usleep(stop);

    test_stat();

}
