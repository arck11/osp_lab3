#include "queue.h"
#include "../tasks/tasks.h"


Queue *createQueue() {
    Queue *q = (Queue *) malloc(sizeof(Queue));
    if (pthread_mutex_init(&(q->lock), NULL) != 0
        || pthread_cond_init(&(q->notify), NULL) != 0) {
        freeQueue(q);
        return NULL;
    }

    q->front = q->back = NULL;
    return q;
}

void freeNode(QNode *node) {
    if (node->value != NULL)
        free(node->value);
    if (node->next != NULL)
        free(node->next);
    if (node->prev != NULL)
        free(node->prev);
    free(node);
}

bool freeQueue(Queue *q) {
    QNode *it, *tmp;

    it = q->front;

    // lock Queue for any change
    pthread_mutex_lock(&(q->lock));

    while (it != NULL) {
        if (it->next != NULL) {
            tmp = it;
            it = it->next;

            tmp->prev = NULL;
            tmp->next = NULL;

            freeNode(tmp);
        } else {
            it->next = NULL;
            it->prev = NULL;
            freeNode(it);
            it = NULL;
        }
    }

    q->front = NULL;
    q->back = NULL;


    pthread_mutex_unlock(&(q->lock));
    pthread_mutex_destroy(&(q->lock));

    free(q);

    return true;
}

bool isEmptyQueue(Queue *q) {
    return q->front == NULL && q->back == NULL;
}

bool isOneQueue(Queue *q) {
    return !isEmptyQueue(q) && q->front == q->back;
}

bool pushQueue(Queue *q, void *message) {
    pthread_mutex_lock(&(q->lock));

    QNode *new_node;

    new_node = (QNode *) malloc(sizeof(QNode));
    new_node->next = NULL;
    new_node->prev = NULL;
    new_node->value = message;

    if (isEmptyQueue(q)) {
        q->front = new_node;
        q->back = new_node;
    } else {
        new_node->next = q->front;
        q->front->prev = new_node;
        q->front = new_node;
    }

    pthread_cond_signal(&(q->notify));
    pthread_mutex_unlock(&(q->lock));

    return true;
}

void *popQueue(Queue *q) {
    pthread_mutex_lock(&(q->lock));
    void *msg = NULL;
    QNode *tmp;

    while (isEmptyQueue(q)) {
        pthread_cleanup_push(pthread_mutex_unlock, &(q->lock))
        pthread_cond_wait(&(q->notify), &(q->lock));
        pthread_cleanup_pop(0)
    }

    msg = q->back->value;
    tmp = q->back;

    if (isOneQueue(q)) {
        q->front = NULL;
        q->back = NULL;
    } else {
        q->back = tmp->prev;
        q->back->next = NULL;
    }

    tmp->next = NULL;
    tmp->prev = NULL;
    tmp->value = NULL;

    pthread_mutex_unlock(&(q->lock));
    return msg;
}
