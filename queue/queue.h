#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include "../tasks/tasks.h"

struct QNode{
    void *value;
    struct QNode *prev, *next;
    pthread_mutex_t lock;
};
typedef struct QNode QNode;

struct Queue{
    pthread_mutex_t lock_push, lock_pop, lock;
    pthread_cond_t notify;
    QNode *front, *back;
};
typedef struct Queue Queue;


Queue *createQueue();
bool freeQueue(Queue *q);
bool isEmptyQueue(Queue *q);
bool isOneQueue(Queue *q);
bool pushQueue(Queue *q, void* message);
void* popQueue(Queue *q);

#endif