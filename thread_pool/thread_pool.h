#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include "../queue/queue.h"

typedef struct {
    void (*function)(void *);
    void *args;
} thread_pool_task;

typedef struct {
    pthread_mutex_t lock;
    pthread_cond_t notify;
    pthread_t *threads;
    Queue* tasks_queue;

    int thread_count;
    int count;
    int shutdown;
    int started;
} thread_pool_t;

typedef enum {
    threadpool_invalid = -1,
    threadpool_lock_failure = -2,
    threadpool_queue_full = -3,
    threadpool_shutdown = -4,
    threadpool_thread_failure = -5
} threadpool_error_t;

typedef enum {
    threadpool_graceful = 1
} threadpool_destroy_flags_t;

typedef enum {
    immediate_shutdown = 1,
    graceful_shutdown = 2
} threadpool_shutdown_t;


static void *thread_pool_thread(void *thread_pool);

int thread_pool_free(thread_pool_t *pool);

thread_pool_t *thread_pool_create(int thread_count);

int thread_pool_add(thread_pool_t *pool, void* (function)(void *), void *arg);

int thread_pool_destroy(thread_pool_t *pool, int flags);

#endif