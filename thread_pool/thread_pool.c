#include <stdlib.h>
#include <pthread.h>
#include "thread_pool.h"




thread_pool_t *thread_pool_create(int thread_count) {
    thread_pool_t *pool;

    pool = (thread_pool_t *) malloc(sizeof(thread_pool_t));

    if (pool == NULL) {
        return NULL; //todo panic
    }

    // init
    pool->thread_count = 0;
    pool->shutdown = pool->started = 0;

    //allocate threads and queues

    pool->threads = (pthread_t*) malloc(sizeof(pthread_t) * thread_count);
    pool->tasks_queue = createQueue();

    if (pool->threads == NULL || pool->tasks_queue == NULL) {
        return NULL; //todo panic
    }

    if (pthread_mutex_init(&(pool->lock), NULL) != 0
        || pthread_cond_init(&(pool->notify), NULL) != 0) {
        return NULL; //todo panic
    }

    for (int i = 0; i < thread_count; ++i) {
        if (pthread_create(&(pool->threads[i]), NULL, thread_pool_thread, (void *) pool) != 0) {
            thread_pool_destroy(pool, 0);
            return NULL;
        }
        pool->thread_count++;
        pool->started++;
    }

    return pool;

}

int thread_pool_add(thread_pool_t *pool, void* (function)(void*), void *argument) {
    int err = 0;

    if (pool == NULL || function == NULL) {
        return threadpool_invalid;
    }

    if (pthread_mutex_lock(&(pool->lock)) != 0) {
        return threadpool_lock_failure;
    }

    do {
        if (pool->shutdown) {
            err = threadpool_shutdown;
            break;
        }

        thread_pool_task *task = (thread_pool_task*) malloc(sizeof(thread_pool_task*));
        task->function = function;
        task->args = argument;

        pushQueue(pool->tasks_queue, (void*)task);
        pool->count += 1;

        if (pthread_cond_signal(&(pool->notify)) != 0) {
            err = threadpool_lock_failure;
            break;
        }
    } while (0);

    if (pthread_mutex_unlock(&pool->lock) != 0) {
        err = threadpool_lock_failure;
    }

    return err;
}

int thread_pool_destroy(thread_pool_t *pool, int flags) {
    int i, err = 0;

    if (pool == NULL) {
        return threadpool_invalid;
    }

    if (pthread_mutex_lock(&(pool->lock)) != 0) {
        return threadpool_lock_failure;
    }

    do {
        /* Already shutting down */
        if (pool->shutdown) {
            err = threadpool_shutdown;
            break;
        }

        pool->shutdown = (flags & threadpool_graceful) ?
                         graceful_shutdown : immediate_shutdown;

        if ((pthread_cond_broadcast(&(pool->notify)) != 0) ||
            (pthread_mutex_unlock(&(pool->lock)) != 0)) {
            err = threadpool_lock_failure;
            break;
        }

        for (i = 0; i < pool->thread_count; i++) {
            if (pthread_join(pool->threads[i], NULL) != 0) {
                err = threadpool_thread_failure;
            }
        }
    } while (0);

    if (!err) {
        thread_pool_free(pool);
    }
    return err;
}

int thread_pool_free(thread_pool_t *pool) {
    if (pool == NULL || pool->started > 0) {
        return -1;
    }

    if (pool->threads) {
        free(pool->threads);
        freeQueue(pool->tasks_queue);

        pthread_mutex_lock(&(pool->lock));
        pthread_mutex_destroy(&(pool->lock));
        pthread_cond_destroy(&(pool->notify));
    }
    free(pool);
    return 0;
}


static void* thread_pool_thread(void *threadpool) {
    thread_pool_t *pool = (thread_pool_t *) threadpool;
    thread_pool_task* task;

    for (;;) {
        pthread_mutex_lock(&(pool->lock));

        while ((pool->count == 0) && (!pool->shutdown)) {
            pthread_cond_wait(&(pool->notify), &(pool->lock));
        }

        if ((pool->shutdown == immediate_shutdown) ||
            ((pool->shutdown == graceful_shutdown) &&
             (pool->count == 0))) {
            break;
        }

        task = (thread_pool_task*)popQueue(pool->tasks_queue);
        pool->count -= 1;

        pthread_mutex_unlock(&(pool->lock));

        (*(task->function))(task->args);
    }

    pool->started--;

    pthread_mutex_unlock(&(pool->lock));
    pthread_exit(NULL);
}